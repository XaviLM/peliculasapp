import { Injectable } from '@angular/core';

import { Jsonp, Http } from '@angular/http';
// import 'rxjs/Rx';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class PeliculasService {
  private apikey = '13973204ba9020df33492f8cd7115de9';
  private urlMoviedb = 'https://api.themoviedb.org/3';
  private imageUrl = 'http://image.tmdb.org/t/p/w500';
  public peliculas: any[] = [];
  public response: string;
  public type = 'movies';
  public actorKnown: any;


  constructor(private jsonp: Jsonp, private http: Http) { }

  getCartelera() {
    console.log('get Cartelera');
    const desde = new Date();
    const hasta = new Date();
    hasta.setDate(hasta.getDate());
    desde.setDate(desde.getDate() - 30);

    const desdeStr = `${desde.getFullYear()}-${desde.getMonth() + 1}-${desde.getDate()}`;
    const hastaStr = `${hasta.getFullYear()}-${hasta.getMonth() + 1}-${hasta.getDate()}`;
    console.log(desdeStr, hastaStr);
// &language=es-ES&region=es&sort_by=release_date.desc&page=1&with_release_type=3

    const url = `${
      this.urlMoviedb
      }/discover/movie?api_key=${
      this.apikey
      }&release_date.gte=${desdeStr}&release_date.lte=${
        hastaStr
      }&language=es-ES&region=es&sort_by=release_date.desc&page=1&with_release_type=3|2&page=1&callback=JSONP_CALLBACK`;
    return this.jsonp.get(url).pipe(map(res => res.json()));
  }
  getPopulares() {

    const url = `${
      this.urlMoviedb
      }/discover/movie?region=es&sort_by=popularity.desc&api_key=${
      this.apikey
      }&language=es&callback=JSONP_CALLBACK`;
    return this.jsonp.get(url).pipe(map(res => res.json()));

  }
  getPopularesNinos() {

    const url = `${
      this.urlMoviedb
      }/discover/movie?certification_country=ES&certification.lte=APTA&sort_by=popularity.desc&api_key=${
      this.apikey
      }&language=es&callback=JSONP_CALLBACK`;
    return this.jsonp.get(url).pipe(map(res => res.json()));

  }

  buscarPelicula(texto: string) {
    this.response = '';
    console.log(texto);
    const url = `${
      this.urlMoviedb
      }/search/movie?query=${
      texto
      }&sort_by=popularity.desc&api_key=${
      this.apikey
      }&language=es&region=ES&callback=JSONP_CALLBACK`;
    // https://api.themoviedb.org/3/search/movie?api_key=<<api_key>>&language=en-US&page=1&include_adult=false

    return this.jsonp.get(url).pipe(map(res => {
      this.peliculas = res.json().results;
      if (!this.peliculas) {
      }
      return res.json().results;
    }));
  }

  buscarActor(texto: string) {

    const url = `${
      this.urlMoviedb
      }/search/person?query=${
      texto
      }&sort_by=popularity.desc&api_key=${
      this.apikey
      }&language=es&include_adult=true&callback=JSONP_CALLBACK`;
    // https://api.themoviedb.org/3/search/movie?api_key=<<api_key>>&language=en-US&page=1&include_adult=false

    return this.jsonp.get(url).pipe(map(res => {
      this.peliculas = res.json().results;
      if (!this.peliculas) {
      }
      return res.json().results;
    }));
  }

  getPelicula(id: string) {

    const url = `${
      this.urlMoviedb
      }/movie/${id}?api_key=${
      this.apikey
      }&language=es&region=ES&callback=JSONP_CALLBACK`;
    return this.jsonp.get(url).pipe(map(res => res.json()));
  }

  detallesActor(id: string) {
    const url = `${
      this.urlMoviedb
      }/person/${
      id
      }?api_key=${
      this.apikey
      }&language=es&region=ES&callback=JSONP_CALLBACK`;
    return this.jsonp.get(url).pipe(map(res => {
      this.peliculas = res.json();
      return res.json();
    }));
  }

  getCasting(id: string) {
    const url = `${
      this.urlMoviedb
      }/movie/
      ${
      id
      }/credits?api_key=${
      this.apikey
      }&language=es&callback=JSONP_CALLBACK`;
    return this.jsonp.get(url).pipe(map(res => res.json()));
  }

  getFilmography(id: string) {
    const url = `${
      this.urlMoviedb
      }/person/${id}/movie_credits?api_key=${
      this.apikey
      }&language=es&region=ESs&sort_by=release_date.desc&callback=JSONP_CALLBACK`;
    return this.jsonp.get(url).pipe(map(res => res.json()));
  }
  getTrailer(id: string) {
    const url = `${
      this.urlMoviedb
      }/movie/${id}/videos?api_key=${
      this.apikey
      }&language=es-ES&callback=JSONP_CALLBACK`;
    return this.jsonp.get(url).pipe(map(res => res.json()));
  }

  getExternalinfo(id: string) {
    // https:api.themoviedb.org/3/find/nm1486647?api_key=13973204ba9020df33492f8cd7115de9&language=es-ES&external_source=imdb_id
    const url = `${this.urlMoviedb}/find/${id}?api_key=${this.apikey}&language=es-ES&external_source=imdb_id&callback=JSONP_CALLBACK`;
    return this.jsonp.get(url).pipe(map(res => res.json()));
  }

  sortBy = function (element: any[], compare: string, direction?: string) {
    let compareList = [];
    const elementSorted = [];
    for (let j = 0; j < element.length; j++) {
        compareList.push(element[j][compare]);
    }
    if (direction === 'desc') {
      compareList = compareList.sort(function (a, b) { return new Date(b).valueOf() - new Date(a).valueOf();  });

    } else {
      compareList = compareList.sort(function (a, b) { return new Date(a).valueOf() - new Date(b).valueOf();   });
    }
    // compareList = compareList.sort();
    for ( let j = 0; j < compareList.length; j++) {
        for (let x = 0; x < element.length; x++) {
            if (compareList[j] === element[x][compare]) {
                elementSorted.push(element[x]);
            }
        }
    }
    return elementSorted;
  };



}
