import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { SearchComponent } from './components/search/search.component';
import { FilmComponent } from './components/film/film.component';
import { ActorComponent } from './components/actor/actor.component';
import { NgModel } from '@angular/forms';

const app_routes: Routes = [
    { path: 'home', component: HomeComponent },
    { path: 'search', component: SearchComponent },
    { path: 'search/:txt', component: SearchComponent },
    { path: 'film/:id/:page', component: FilmComponent },
    { path: 'actor/:id/:page', component: ActorComponent },
    { path: '**', component: HomeComponent },
];

export const APP_ROUTING = RouterModule.forRoot(app_routes, {useHash: true});
