import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'peliculasImagen'
})
export class PeliculasImagenPipe implements PipeTransform {


  transform(pelicula: any, poster: boolean = false): any {

    const imageUrl: string = 'http://image.tmdb.org/t/p/w500';
    if (poster && pelicula.poster_path) {
      return imageUrl + pelicula.poster_path;
    }
    if (pelicula.backdrop_path) {
      return imageUrl + pelicula.backdrop_path;

    } else if (pelicula.poster_path) {
      return imageUrl + pelicula.poster_path;

    } else if (pelicula.profile_path) {
      return imageUrl + pelicula.profile_path;

    } else {
      return 'assets/no-image.jpg';
    }
  }

}
