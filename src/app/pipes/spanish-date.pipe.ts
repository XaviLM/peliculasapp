import { Pipe, PipeTransform } from '@angular/core';
import { stringify } from '@angular/core/src/util';

@Pipe({
  name: 'spanishDate'
})
export class SpanishDatePipe implements PipeTransform {

  transform(value: string, ): any {
    let month;
    let year;
    let day;
let control: any;
if (value) {
  control = value.indexOf('-');
  year = value.slice(0, control);
  // control = control + 1;
  month = value.slice(control + 1);
  control = month.indexOf('-');
  day = month;
  month = month.slice(0, control);
  // control = control + 1;
  day = day.slice(control + 1);
  return day + '-' + month + '-' + year;
} else {
  return 'Sin fecha';
}

}


}
