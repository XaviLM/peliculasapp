import { Component } from '@angular/core';
import { PeliculasService } from './providers/peliculas.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Cartelera-Versión-Crítica';

  constructor(public _ps: PeliculasService) { }


}
