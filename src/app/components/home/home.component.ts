import { Component, OnInit } from '@angular/core';
import { PeliculasService } from '../../providers/peliculas.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: []
})
export class HomeComponent implements OnInit {
  cartelera: any;
  populares: any;
  popNinos: any;
  ps = 0; // page start
  pe = 10; // page end


  constructor(public _ps: PeliculasService) {

    this._ps.getCartelera()
      .subscribe(data => {
        this.cartelera = data.results;
        // console.log('cartelera before', this.cartelera);
        // this.cartelera = _ps.sortBy(this.cartelera, 'release_date', 'desc');
        console.log('cartelera after', this.cartelera);
      });

    this._ps.getPopulares()
      .subscribe(data => {
        this.populares = data.results;
        // console.log('populares:', this.populares);
      });

    this._ps.getPopularesNinos()
      .subscribe(data => {
        this.popNinos = data.results;
        // console.log('populares niños:', this.popNinos);
      });
  }
  more() {
    if (this.pe < this.cartelera.length) {
      this.pe = this.pe + 10;
    }
  }
  ngOnInit() {
  }

}
