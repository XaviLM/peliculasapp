import { Component, OnInit } from '@angular/core';
import { PeliculasService } from 'src/app/providers/peliculas.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-actor',
  templateUrl: './actor.component.html',
  styleUrls: ['./actor.component.scss']
})
export class ActorComponent implements OnInit {

  actor: any;
  back: string;
  filmografia: any;
  pe_film = 10;
  pe_tecnic = 10;
  endFilm = true;
  endTecnic = true;
  moreBio = false;



  constructor(public _ps: PeliculasService, public route: ActivatedRoute, private go: Router, public location: Location ) {

    this.route.params.subscribe(params => {

      this.back = params.page;

      _ps.detallesActor(params.id).subscribe(data => {
        this.actor = data;
        console.log('detalles actor', data);
        _ps.getFilmography(this.actor.id).subscribe(films => {
          this.filmografia = films;
           this.filmografia.cast = _ps.sortBy(this.filmografia.cast, 'release_date', 'desc');
          this.filmografia.crew = _ps.sortBy(this.filmografia.crew, 'release_date', 'desc');
          _ps.getExternalinfo(this.actor.imdb_id).subscribe(extra => {
            console.log('extra imdb', extra.person_results);
          });

          this.startPagination();
          // console.log('filmografía', this.filmografia);

        });
      });
    });
  }
  ngOnInit() {



  }
  goBack() {

    this.location.back();
  }
  more( data) {
    if (data === 'crew') {
      if (this.pe_tecnic <= this.filmografia.crew.length) {
        this.pe_tecnic = this.pe_tecnic + 10;
        this.startPagination();
      } else {
        this.endFilm = true;
      }
    } else {
      if (this.pe_film <= this.filmografia.cast.length) {
        this.pe_film = this.pe_film + 10;
        this.startPagination();
      } else {
        this.endFilm = true;
      }

    }

  }

  startPagination() {
    if (this.filmografia.cast) {
      if (this.pe_film <= this.filmografia.cast.length) {
        this.endFilm = false;
      } else {
        this.endFilm = true;
      }
    } else {
      this.endFilm = true;
    }

    if (this.filmografia.crew) {
      if (this.pe_tecnic <= this.filmografia.crew.length) {
        this.endTecnic = false;
      } else {
        this.endTecnic = true;
      }
    } else {
      this.endTecnic = true;
    }
  }
toggleMoreBio() {
  this.moreBio = !this.moreBio;
}


}
