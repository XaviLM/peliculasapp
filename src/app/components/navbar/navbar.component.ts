import { Component, OnInit } from '@angular/core';
import { PeliculasService } from '../../providers/peliculas.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styles: []
})
export class NavbarComponent implements OnInit {

  constructor(private route: Router) { }

  searchFim(value: string) {
    if (!value || value.length === 0) {
      return;
    }
    this.route.navigate(['search', value]);
  }

  ngOnInit() {
  }

}
