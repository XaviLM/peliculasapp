import { Component, OnInit } from '@angular/core';
import { PeliculasService } from '../../providers/peliculas.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styles: []
})
export class SearchComponent implements OnInit {
  buscar: string;
  // type = 'movies'; // movies or actor
  response: string = null;

  constructor(public _ps: PeliculasService, public route: ActivatedRoute) {
    this.route.params.subscribe(params => {
      console.log(params);
      if (params['txt']) {
        this.buscar = params.txt;
        console.log(this.buscar);
        this.searchFilm();
      }
    });
  }

  search() {
    if (this._ps.type === 'movies') {
      this.searchFilm();
    } else {
      this.searchActor();
    }
  }

  searchFilm() {
    this.response = null;
    if (!this.buscar || this.buscar.length === 0) {
      return;
    }
    this._ps.buscarPelicula(this.buscar)
      .subscribe(data => {
        if (data.length < 1) {
          this.response = 'Desgraciadamente no hemos encontrado lo que buscabas';
        }
        console.log('resultado:', data);
      });
  }
  searchActor() {
    this.response = null;
    if (!this.buscar || this.buscar.length === 0) {
      return;
    }
    this._ps.buscarActor(this.buscar)
      .subscribe(data => {
        if (data.length < 1) {
          this.response = 'Desgraciadamente no hemos encontrado lo que buscabas';
        }
        console.log('resultado:', data);
      });

  }

  masDetalles(id: string) {

    this._ps.detallesActor(id)
      .subscribe(data => {
        console.log('resultado detalles:', data);
      });

  }

  ngOnInit() {
  }

}
