import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PeliculasService } from '../../providers/peliculas.service';
import { Location } from '@angular/common';




@Component({
  selector: 'app-film',
  templateUrl: './film.component.html',
  styles: []
})
export class FilmComponent implements OnInit {
  film: any;
  back: string;
  casting: any;
  resumen: any = null;


  constructor(public _ps: PeliculasService, public route: ActivatedRoute, private go: Router, public location: Location) {
    this.route.params.subscribe(params => {
      // console.log(params);
      this.back = params.page;
      _ps.getPelicula(params.id).subscribe(data => {
        this.film = data;
        // console.log('pelicula: ', data);
        _ps.getTrailer(params.id).subscribe(trailer => {
          this.resumen = trailer.results;
          // console.log('trailer: ', trailer);
        });
        _ps.getCasting(data.id).subscribe(cast => {
          this.casting = cast;
          // console.log('casting:', this.casting);
        });
      });
    });
  }

  goBack() {
    this.location.back();
  }


  ngOnInit() {
  }

}
