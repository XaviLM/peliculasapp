import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { PeliculasService } from './providers/peliculas.service';
import { HttpModule, JsonpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { HomeComponent } from './components/home/home.component';
import { FilmComponent } from './components/film/film.component';
import { SearchComponent } from './components/search/search.component';
import { APP_ROUTING } from './app.routes';
import { PeliculasImagenPipe } from './pipes/peliculas-imagen.pipe';
import { GaleriaComponent } from './components/home/galeria.component';
import { SpanishDatePipe } from './pipes/spanish-date.pipe';
import { FooterComponent } from './components/footer/footer.component';
import { ActorComponent } from './components/actor/actor.component';
import { TrailerPipe } from './pipes/trailer.pipe';
import { DomSanitizer } from '@angular/platform-browser';



@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    FilmComponent,
    SearchComponent,
    PeliculasImagenPipe,
    GaleriaComponent,
    SpanishDatePipe,
    FooterComponent,
    ActorComponent,
    TrailerPipe
  ],
  imports: [
    BrowserModule,
    JsonpModule,
    HttpModule,
    FormsModule,
    APP_ROUTING
  ],
  providers: [PeliculasService],
  bootstrap: [AppComponent]
})
export class AppModule {}
